package com.njitram.ubiquity.controller.autohotkey;

import java.io.IOException;

public class AutoHotKeyController {
	public static void runAhkRestScript() {
		try {
			Runtime.getRuntime().exec(String.format("scripts/AutoHotkey.exe scripts/Ubiquity_rest.ahk"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
