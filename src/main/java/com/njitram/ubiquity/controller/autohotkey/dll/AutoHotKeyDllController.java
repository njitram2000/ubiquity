package com.njitram.ubiquity.controller.autohotkey.dll;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.sun.jna.Native;
import com.sun.jna.WString;

/*
 * Note that this can execute AHK commands and receive a result but cannot keep a script running for key capture (AFAIK)
 * Because of this, it is not used in Ubiquity at the moment but kept here as reference code
 */
public class AutoHotKeyDllController {
	private AutoHotKeyDll lib;

	public AutoHotKeyDllController() {
		lib = (AutoHotKeyDll) Native.loadLibrary("autohotkey/AutoHotkey_H_v1", AutoHotKeyDll.class);
//		lib.ahktextdll(new WString("#Persistent"));
		lib.ahktextdll(new WString(""),new WString(""),new WString(""));
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
		}
	}

	public void testMsg() {
		lib.ahkExec(new WString("msgbox,,Hellow!,Hellow World!"));
	}
	
	public void runRestAhk() {
		lib.addScript(readRestAhk(), 1);
	}

	private WString readRestAhk() {
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(getClass().getResource("/autohotkey/Ubiquity_rest.ahk").toURI()), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		return new WString(contentBuilder.toString());
	}
	
	public static void main(String[] argv) {
		new AutoHotKeyDllController().runRestAhk();
		try {
			Thread.sleep(1000000000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
