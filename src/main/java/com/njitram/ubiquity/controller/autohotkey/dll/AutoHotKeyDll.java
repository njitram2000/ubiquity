package com.njitram.ubiquity.controller.autohotkey.dll;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.WString;

public interface AutoHotKeyDll extends Library {
	public void ahkExec(WString s);

	public void ahkdll(WString s, WString o, WString p);

	public void addFile(WString s, int a);
	public void addFile(WString s, int a, int b);
	
	public void addScript(WString s, int a);

	public void ahktextdll(WString s, WString o, WString p);
	public void ahktextdll(WString s);

	public Pointer ahkFunction(WString f, WString p1, WString p2, WString p3, WString p4, WString p5, WString p6,
			WString p7, WString p8, WString p9, WString p10);
}
