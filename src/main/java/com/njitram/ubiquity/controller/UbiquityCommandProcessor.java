package com.njitram.ubiquity.controller;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.apache.commons.lang3.StringUtils;

import com.njitram.ubiquity.controller.command.GoogleTranslateCommand;
import com.njitram.ubiquity.controller.command.JavascriptCommands;
import com.njitram.ubiquity.controller.command.NoJSCommandException;
import com.njitram.ubiquity.controller.command.OxfordDefinitionCommand;
import com.njitram.ubiquity.controller.command.UbiquityCommand;
import com.njitram.ubiquity.view.UbiquityCommandField;
import com.njitram.ubiquity.view.UbiquityFrame;
import com.njitram.ubiquity.view.UbiquityPreview;

import spark.Request;
import spark.Response;
import spark.Spark;

public class UbiquityCommandProcessor {
	public static final boolean DEBUG = false;
	
	private static final String UP_ACTION = "UP_ACTION";
	private static final String DOWN_ACTION = "DOWN_ACTION";
	private static final String ENTER_ACTION = "ENTER_ACTION";
	
	private UbiquityFrame ubiquityFrame;
	private List<UbiquityCommand> ubiquityCommands = new ArrayList<UbiquityCommand>();
	
	/**
	 * Populate the list with all the possible Ubiquity commands
	 * The order they are added is the order in which they are discovered
	 */
	{
		ubiquityCommands.add(new OxfordDefinitionCommand());
		ubiquityCommands.add(new GoogleTranslateCommand());
	}
	
	public UbiquityCommandProcessor(UbiquityFrame ubiquityFrame) {
		this.ubiquityFrame = ubiquityFrame;
		
		if(DEBUG) {
			Spark.port(4567);
		} else {
			Spark.port(4568);
		}
		Spark.get("/show", (req, res) -> show(req, res));
		
		UbiquityCommandField commandInput = ubiquityFrame.getCommandInput();
		commandInput.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) { 
				onKeyPress();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				onKeyPress();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
			}
		});
		
		/*
		 * Set special key strokes for input field
		 */
		KeyStroke upKeyStroke = KeyStroke.getKeyStroke("UP");
		KeyStroke downKeyStroke = KeyStroke.getKeyStroke("DOWN");
		KeyStroke enterKeyStroke = KeyStroke.getKeyStroke("ENTER");
		InputMap im = commandInput.getInputMap();
		im.put(enterKeyStroke, ENTER_ACTION);
		im.put(upKeyStroke, UP_ACTION);
		im.put(downKeyStroke, DOWN_ACTION);
		ActionMap actionMap = commandInput.getActionMap();
		actionMap.put(ENTER_ACTION, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				onEnter();
			}
		});
		actionMap.put(UP_ACTION, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("up");
			}
		});
		actionMap.put(DOWN_ACTION, new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("down");
			}
		});
	    
	    ubiquityFrame.addWindowFocusListener(new WindowFocusListener() {
			@Override
			public void windowLostFocus(WindowEvent e) {
				ubiquityFrame.hideUI();
			}
			
			@Override
			public void windowGainedFocus(WindowEvent e) {
			}
		});
	}
	
	private void onEnter() {
		String commandText = ubiquityFrame.getCommandInput().getText();
		if(StringUtils.isEmpty(commandText)) {
			return;
		}
		UbiquityCommandInput commandInput = new UbiquityCommandInput(commandText);
		
		String processResult = null;
		try {
			JavascriptCommands jsCommands = new JavascriptCommands();
			processResult = jsCommands.process(commandInput.getAbbreviatedName(), commandInput.getArgs());
		} catch(NoJSCommandException e) {
			UbiquityCommand command = findSuitableCommand(commandInput.getAbbreviatedName());
			if(command != null) {
				processResult = command.process(commandInput.getArgs());
			}
		}
		
		if(processResult == null) {
			ubiquityFrame.hideUI();
		} else {
			showPreview(processResult);
		}
	}
	
	private void onKeyPress() {
		showPreview();
	}
	
	private void showPreview() {
		String commandText = ubiquityFrame.getCommandInput().getText();
		UbiquityCommandInput commandInput = new UbiquityCommandInput(commandText);
		
		String previewResult = null;
		try {
			JavascriptCommands jsCommands = new JavascriptCommands();
			previewResult = jsCommands.getInputPreview(commandInput.getAbbreviatedName(), commandInput.getArgs());
		} catch(NoJSCommandException e) {
			UbiquityCommand command = findSuitableCommand(commandInput.getAbbreviatedName());
			if(command != null) {
				previewResult = command.getInputPreview(commandInput.getArgs());
			}
		}
		
		if(previewResult != null) {
			showPreview(previewResult);
		}
	}
	
	private void showPreview(String previewText) {
		if(previewText != null) {
			UbiquityPreview preview = ubiquityFrame.getPreview();
			preview.resetPane();
			preview.appendToPane(previewText);
		}
	}
	
	private UbiquityCommand findSuitableCommand(String abbreviatedName) {
		for(UbiquityCommand command : ubiquityCommands) {
			if(command.matchesAbbreviatedName(abbreviatedName)) {
				return command;
			}
		}
		return null;
	}
	
	private String show(Request request, Response response) {
		String initialCommand = request.queryParams("input");
		ubiquityFrame.showUI();
		if(!StringUtils.isEmpty(initialCommand)) {
		    ubiquityFrame.getCommandInput().setText(" " + initialCommand);
		    ubiquityFrame.getCommandInput().setCaretPosition(0);
		    showPreview();
	    } else {
	    	ubiquityFrame.getCommandInput().selectAll();
	    }
		return "";
	}
}
