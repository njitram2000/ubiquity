package com.njitram.ubiquity.controller;

import java.util.Arrays;
import java.util.List;

public class UbiquityCommandInput {
	private String abbreviatedName;
	private List<String> args;
	
	public UbiquityCommandInput(String commandText) {
		String[] commandParts = commandText.split(" ");
		abbreviatedName = commandParts[0];
		for(int i=0; i<commandParts.length; i++) {
			commandParts[i] = commandParts[i].replace("'","").replace("\"","");
		}
		args = Arrays.asList(Arrays.copyOfRange(commandParts, 1, commandParts.length));
	}
	

	public String getAbbreviatedName() {
		return abbreviatedName;
	}

	public List<String> getArgs() {
		return args;
	}
}
