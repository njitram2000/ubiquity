package com.njitram.ubiquity.controller.command;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.text.StringEscapeUtils;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.Function;
import org.mozilla.javascript.Scriptable;

import com.njitram.ubiquity.log.Logger;

/*
 * https://github.com/mozilla/rhino/tree/master/examples
 * https://developer.mozilla.org/en-US/docs/Mozilla/Projects/Rhino
 * https://mvnrepository.com/artifact/org.mozilla/rhino/1.7.11
 * https://ringojs.org/documentation/rhino_hacker_guide/
 */
public class JavascriptCommands {
	private static final String UBIQUITY_SCRIPT_JS = "scripts/ubiquity_script.js";

	Context cx;
	Scriptable scope;
	Scriptable ubiquity;

	private void init() {
		cx = Context.enter();
		scope = cx.initStandardObjects();
		try {
			cx.evaluateReader(scope, new FileReader(UBIQUITY_SCRIPT_JS), UBIQUITY_SCRIPT_JS, 1, null);
			ubiquity = ((Function) scope.get("Ubiquity", scope)).construct(cx, scope, null);
		} catch (IOException e) {
			Logger.error(e);
			e.printStackTrace();
		}
	}

	public String getInputPreview(String abbreviatedName, List<String> args) throws NoJSCommandException {
		args = escapeArgs(args);
		init();
		if (hasCommand(abbreviatedName)) {
			return (String) ((Function) ubiquity.get("preview", ubiquity)).call(cx, scope, scope,
					new Object[] { abbreviatedName, args.toArray() });
		} else {
			throw new NoJSCommandException();
		}
	}

	public String process(String abbreviatedName, List<String> args) throws NoJSCommandException {
		args = escapeArgs(args);
		init();
		if (hasCommand(abbreviatedName)) {
			String resultUrl = (String) ((Function) ubiquity.get("process", ubiquity)).call(cx, scope, scope,
					new Object[] { abbreviatedName, args.toArray() });
			return UbiquityCommand.openUrl(resultUrl, null, "Failed to open url");
		} else {
			throw new NoJSCommandException();
		}
	}

	private List<String> escapeArgs(List<String> args) {
		ArrayList<String> escapedArgs = new ArrayList<String>();
		for (String arg : args) {
			escapedArgs.add(StringEscapeUtils.escapeEcmaScript(arg));
		}
		return escapedArgs;
	}
	
	private boolean hasCommand(String command) {
		Boolean hasCommand = (Boolean) ((Function) ubiquity.get("hasCommand", ubiquity)).call(cx, scope, scope,
				new Object[] { command });
		return Boolean.TRUE.equals(hasCommand);
	}

}
