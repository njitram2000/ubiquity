package com.njitram.ubiquity.controller.command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.njitram.ubiquity.log.Logger;

public class GoogleTranslateCommand extends UbiquityCommand {
	private static final String API_URL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=%s&tl=%s&dt=t&q=%s";
//	private static final String API_URL = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=nl&tl=en&dt=t&q=emmer";

	@Override
	public List<String> getNames() {
		return Arrays.asList(new String[] { "translate" });
	}

	@Override
	public String getInputPreview(List<String> args) {
		Object[] inputArray = parseInput(args);
		return String.format("Press enter to translate from %s to %s: \n%s", inputArray);
	}

	@Override
	public String process(List<String> args) {
		String[] inputArray = parseInput(args);
		String sourceLang = inputArray[0];
		String targetLang = inputArray[1];
		String phrase = inputArray[2];
		
		try {
			URL url = new URL(String.format(API_URL, sourceLang, targetLang, URLEncoder.encode(StringUtils.join(phrase, " ").replace("\\n", " "), "UTF-8")));
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
			con.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer content = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			String json = content.toString();
			ObjectMapper objectMapper = new ObjectMapper();
			List array = objectMapper.readValue(json, List.class);
			ArrayList<String> sentences = new ArrayList<String>();
			for(Object translations : (List)array.get(0)) {
				String sentence = (String)((List)translations).get(0);
				sentences.add(sentence);
			}
			String translation = StringUtils.join(sentences, "");
			return translation;
		} catch (IOException e) {
			e.printStackTrace();
			Logger.error(e);
			return "Unable to translate";
		}
	}
	
	private String[] parseInput(List<String> input) {
		String sourceLang = "auto";
		String targetLang = "auto";
		List<String> phrase = new ArrayList<String>();
		phrase.addAll(input);
		for(int i=0; i<2; i++) {
			if(phrase.size() > 1) {
				if(phrase.get(0).equalsIgnoreCase("from")) {
					sourceLang = phrase.get(1).toLowerCase();
					phrase.remove(0);
					phrase.remove(0);
				} else if(phrase.get(0).equalsIgnoreCase("to")) {
					targetLang = phrase.get(1).toLowerCase();
					phrase.remove(0);
					phrase.remove(0);
				}
			}
		}
		return new String[]{sourceLang, targetLang, StringUtils.join(phrase, " ")};
	}
}
