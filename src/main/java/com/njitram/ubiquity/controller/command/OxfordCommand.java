package com.njitram.ubiquity.controller.command;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public abstract class OxfordCommand extends UbiquityCommand {
	protected static final String API_URL = "https://od-api.oxforddictionaries.com/api/v2";
	protected static final String APP_ID = "e96c8b11";
	protected static final String APP_KEY = "d9c0006e43872373fbf90bd0166daaac";
	protected static final String LANG_EN = "en";
	
	protected Client client;
	protected WebTarget webTarget;
	
	public OxfordCommand() {
		client = ClientBuilder.newClient();
		webTarget = client.target(API_URL);
	}
}
