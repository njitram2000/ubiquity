package com.njitram.ubiquity.controller.command;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;

import com.njitram.ubiquity.log.Logger;
import com.njitram.ubiquity.model.command.oxford.definition.Entry;
import com.njitram.ubiquity.model.command.oxford.definition.LexicalEntry;
import com.njitram.ubiquity.model.command.oxford.definition.OxfordDefinition;
import com.njitram.ubiquity.model.command.oxford.definition.Sense;

/**
 * https://developer.oxforddictionaries.com/documentation#!/Dictionary32entries/get_entries_source_lang_word_id
 * https://developer.oxforddictionaries.com/version2
 * @author Martijn
 *
 */

public class OxfordDefinitionCommand extends OxfordCommand {
	
	@Override
	public List<String> getNames() {
		return Arrays.asList(new String[] {"dictionary","definition"});
	}

	@Override
	public String getInputPreview(List<String> args) {
		return "Press enter to search for a definition of " + StringUtils.join(args, " ");
	}

	@Override
	public String process(List<String> args) {
		try {
			WebTarget defWebTarget = webTarget.path("entries/" + LANG_EN + "/" + URLEncoder.encode(StringUtils.join(args, " "), "UTF-8"));
			Invocation.Builder invocationBuilder = defWebTarget.request(MediaType.APPLICATION_JSON);
			invocationBuilder.header("app_id", APP_ID);
			invocationBuilder.header("app_key", APP_KEY);
			OxfordDefinition oxfordDefinition = invocationBuilder.get(OxfordDefinition.class);
			List<String> definitions = new ArrayList<String>();
			if(oxfordDefinition.getResults().size() > 0) {
				for(LexicalEntry lexicalEntry : oxfordDefinition.getResults().get(0).getLexicalEntries()) {
					for(Entry entry : lexicalEntry.getEntries()) {
						for(Sense sense : entry.getSenses()) {
							if(sense.getDefinitions() != null) {
								for(String definition : sense.getDefinitions()) {
									definitions.add(definition);
								}
							}
						}
					}
				}
			}
			
			StringBuilder sb = new StringBuilder();
			int counter = 1;
			for(String def : definitions) {
				sb.append(counter).append(") ").append(def).append("\n\n");
				counter++;
			}
			return sb.toString();
		} catch (UnsupportedEncodingException | ProcessingException | WebApplicationException e) {
			Logger.error(e);
			return "No definition for " + StringUtils.join(args, " ");
		}
	}
}
