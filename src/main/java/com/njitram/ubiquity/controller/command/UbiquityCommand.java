package com.njitram.ubiquity.controller.command;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.njitram.ubiquity.log.Logger;

public abstract class UbiquityCommand {
	protected static final String QUERY_PLACEHOLDER = "{query}";
	
	abstract public List<String> getNames();
	public boolean matchesAbbreviatedName(String abbreviatedName) {
		for(String name : getNames()) {
			if(name.toLowerCase().contains(abbreviatedName.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	abstract public String getInputPreview(List<String> args);
	/**
	 * Called when enter is pressed
	 * @param args
	 * @return null to close the application or a string to be displayed in the preview area
	 */
	abstract public String process(List<String> args);
	
	protected static String openUrl(String url, List<String> args, String errorText) {
		try {
			if(url.contains(QUERY_PLACEHOLDER)) {
				url = url.replace(QUERY_PLACEHOLDER, URLEncoder.encode(StringUtils.join(args, " "), "UTF-8"));
			}
			URI uri = new URI(url);
			/*
			 * Switch back to the source app, then open the url. This way, the source app is in focus when the url is opened
			 */
			java.awt.Desktop.getDesktop().browse(uri);
			/*
			 * Sleep a bit to allow focus to switch to the browser
			 */
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
				Logger.error(e);
			}
			return null;
		} catch (URISyntaxException | IOException e) {
			/*
			 * Switch back to Ubiquity to show the error
			 */
			Logger.error(e);
			return errorText;
		}
	}
}
