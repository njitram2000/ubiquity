package com.njitram.ubiquity.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import com.njitram.ubiquity.controller.autohotkey.AutoHotKeyController;
import com.njitram.ubiquity.view.UbiquityFrame;

public class UbiquityMain {

	public static void main(String[] argv) {
		try {
			PrintStream writetofile = new PrintStream(
				     new FileOutputStream("ubiquity.log", true));
			System.setOut(writetofile);
			System.setErr(writetofile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// Run the autohotkey script to capture ctrl+space and send rest command to Ubiquity
		AutoHotKeyController.runAhkRestScript();
		// Create the Ubiquity GUI
		UbiquityFrame ubiquityFrame = new UbiquityFrame();
		// Start the ubiquity rest server
		new UbiquityCommandProcessor(ubiquityFrame);
	}
}
