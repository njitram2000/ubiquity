
package com.njitram.ubiquity.model.command.oxford.definition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "definitions",
    "id",
    "shortDefinitions",
    "subsenses",
    "domains",
    "examples",
    "thesaurusLinks",
    "crossReferenceMarkers",
    "crossReferences",
    "regions",
    "registers"
})
public class Sense {

    @JsonProperty("definitions")
    private List<String> definitions = null;
    @JsonProperty("id")
    private String id;
    @JsonProperty("shortDefinitions")
    private List<String> shortDefinitions = null;
    @JsonProperty("subsenses")
    private List<Subsense> subsenses = null;
    @JsonProperty("domains")
    private List<Domain_> domains = null;
    @JsonProperty("examples")
    private List<Example_> examples = null;
    @JsonProperty("thesaurusLinks")
    private List<ThesaurusLink_> thesaurusLinks = null;
    @JsonProperty("crossReferenceMarkers")
    private List<String> crossReferenceMarkers = null;
    @JsonProperty("crossReferences")
    private List<CrossReference> crossReferences = null;
    @JsonProperty("regions")
    private List<Region> regions = null;
    @JsonProperty("registers")
    private List<Register> registers = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("definitions")
    public List<String> getDefinitions() {
        return definitions;
    }

    @JsonProperty("definitions")
    public void setDefinitions(List<String> definitions) {
        this.definitions = definitions;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("shortDefinitions")
    public List<String> getShortDefinitions() {
        return shortDefinitions;
    }

    @JsonProperty("shortDefinitions")
    public void setShortDefinitions(List<String> shortDefinitions) {
        this.shortDefinitions = shortDefinitions;
    }

    @JsonProperty("subsenses")
    public List<Subsense> getSubsenses() {
        return subsenses;
    }

    @JsonProperty("subsenses")
    public void setSubsenses(List<Subsense> subsenses) {
        this.subsenses = subsenses;
    }

    @JsonProperty("domains")
    public List<Domain_> getDomains() {
        return domains;
    }

    @JsonProperty("domains")
    public void setDomains(List<Domain_> domains) {
        this.domains = domains;
    }

    @JsonProperty("examples")
    public List<Example_> getExamples() {
        return examples;
    }

    @JsonProperty("examples")
    public void setExamples(List<Example_> examples) {
        this.examples = examples;
    }

    @JsonProperty("thesaurusLinks")
    public List<ThesaurusLink_> getThesaurusLinks() {
        return thesaurusLinks;
    }

    @JsonProperty("thesaurusLinks")
    public void setThesaurusLinks(List<ThesaurusLink_> thesaurusLinks) {
        this.thesaurusLinks = thesaurusLinks;
    }

    @JsonProperty("crossReferenceMarkers")
    public List<String> getCrossReferenceMarkers() {
        return crossReferenceMarkers;
    }

    @JsonProperty("crossReferenceMarkers")
    public void setCrossReferenceMarkers(List<String> crossReferenceMarkers) {
        this.crossReferenceMarkers = crossReferenceMarkers;
    }

    @JsonProperty("crossReferences")
    public List<CrossReference> getCrossReferences() {
        return crossReferences;
    }

    @JsonProperty("crossReferences")
    public void setCrossReferences(List<CrossReference> crossReferences) {
        this.crossReferences = crossReferences;
    }

    @JsonProperty("regions")
    public List<Region> getRegions() {
        return regions;
    }

    @JsonProperty("regions")
    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    @JsonProperty("registers")
    public List<Register> getRegisters() {
        return registers;
    }

    @JsonProperty("registers")
    public void setRegisters(List<Register> registers) {
        this.registers = registers;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
