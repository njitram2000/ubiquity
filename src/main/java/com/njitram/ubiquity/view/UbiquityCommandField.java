package com.njitram.ubiquity.view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class UbiquityCommandField extends JTextField {
	private static final int BORDER_SIZE = 10;
	public UbiquityCommandField() {
		Font font = new Font("SansSerif", Font.BOLD, 22);
		setFont(font);
		setBackground(Color.LIGHT_GRAY);
		setBorder(BorderFactory.createCompoundBorder(
		        getBorder(), 
		        BorderFactory.createEmptyBorder(BORDER_SIZE,BORDER_SIZE,BORDER_SIZE,BORDER_SIZE)));
	}
}
