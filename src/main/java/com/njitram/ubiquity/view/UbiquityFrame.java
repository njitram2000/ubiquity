package com.njitram.ubiquity.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

import com.njitram.ubiquity.controller.UbiquityCommandProcessor;

@SuppressWarnings("serial")
public class UbiquityFrame extends JFrame {
	private UbiquityCommandField commandInput = new UbiquityCommandField();
	private UbiquityPreview preview = new UbiquityPreview();

	public UbiquityFrame() {
		super("Ubiquity");
		setUndecorated(true); // Remove title bar
		Container c = getContentPane();
		c = getContentPane();
		c.setPreferredSize(new Dimension(800, 500));
		c.setLayout(new BorderLayout());
		c.add(commandInput, BorderLayout.NORTH);
		c.add(preview, BorderLayout.CENTER);

		mapCloseBehaviour();

		pack();
		showUI();
		if (!UbiquityCommandProcessor.DEBUG) {
			hideUI();
		}
	}

	private void mapCloseBehaviour() {
		// Set Esc key to shut down app
		KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
		Action escapeAction = new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				hideUI();
			}
		};
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "ESCAPE");
		getRootPane().getActionMap().put("ESCAPE", escapeAction);
	}

	public UbiquityCommandField getCommandInput() {
		return commandInput;
	}

	public UbiquityPreview getPreview() {
		return preview;
	}

	public void hideUI() {
		setVisible(false);
	}

	public void showUI() {
		setVisible(true);
		setVisible(true);
	}

	public void disposeAndExit() {
		setVisible(false);
		dispose();
		System.exit(0);
	}
}
