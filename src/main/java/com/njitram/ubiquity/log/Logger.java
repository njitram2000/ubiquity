package com.njitram.ubiquity.log;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger {
	private static final String ERROR_FILE = "error.log";
	private static final SimpleDateFormat sdtf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
	
	public static void error(Exception e) {
		e.printStackTrace();
		try (PrintWriter pw = new PrintWriter(new FileWriter(ERROR_FILE, true))) {
			pw.println("\r\n["+sdtf.format(Calendar.getInstance().getTime())+"]");
			e.printStackTrace(pw);
		} catch (IOException ioe) {
			System.err.println("IOException: " + ioe.getMessage());
		}
	}
	
	public static void error(String msg) {
		try (PrintWriter pw = new PrintWriter(new FileWriter(ERROR_FILE, true))) {
			pw.println("\r\n["+sdtf.format(Calendar.getInstance().getTime())+"]");
			pw.println(msg);
		} catch (IOException ioe) {
			System.err.println("IOException: " + ioe.getMessage());
		}
		
	}
}
