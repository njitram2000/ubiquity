#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

GroupAdd, GroupName, ahk_exe firefox.exe
GroupAdd, GroupName, ahk_exe thunderbird.exe
GroupAdd, GroupName, ahk_exe Explorer.EXE
GroupAdd, GroupName, ahk_exe iTunes.exe

$^Space::
	IfWinActive, ahk_group GroupName
	{
		clipsaved:= ClipboardAll    ; This line is here so the original clipboard contents can be restored when the script is finished.
		Clipboard =    ; This erases the clipboard, so that we can be sure something new is added in the next step.
		Send, ^c    ; Add the highlighted text to the clipboard
		Sleep 150  ; Give Windows time to actually populate the clipboard - you may need to experiment with the time here.
		
		; %Clipboard%
		url := "http://localhost:4568/show?input=" . Clipboard
		oWhr := ComObjCreate("WinHttp.WinHttpRequest.5.1")
		oWhr.Open("GET", url, false)
		oWhr.Send()

		Clipboard := clipsaved    ; Sets the clipboard back to whatever was on it before you ran this script.

		; Because sometimes the Ubiquity window is not focussed but just flashes in the background
		If WinExist("Ubiquity ahk_class SunAwtFrame")
		{
			Sleep, 200 ; just to be sure
			WinActivate
		}
	}
	else
	{
		Send, ^{space}
	}
return

^&::
	
return