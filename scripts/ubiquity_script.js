function join(arr, sep) {
	var string = "";
	for(i in arr) {
		if(string !== "") {
			string += sep;
		}
		string += arr[i];
	}
	return string
}

function Ubiquity() {
	var ubiquity = this;
	var QUERY = "{query}";
	
	/*
	 * Complete this list with all the commands below
	 */ 
	this.commands = [
		new function() {
			var me = this;
			this.names = ["google"];
			this.url = "https://www.google.com/search?q={query}";
			this.preview = function(input) {
				return "Search Google for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["youtube"];
			this.url = "https://www.youtube.com/results?search_type=search_videos&search=Search&search_sort=relevance&search_query={query}";
			this.preview = function(input) {
				return "Search Youtube for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["piratebay"];
			this.url = "https://boatbay.click/s/?q={query}&page=0&orderby=99";
			this.preview = function(input) {
				return "Search Piratebay for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["imdb"];
			this.url = "https://www.imdb.com/find?q={query}";
			this.preview = function(input) {
				return "Search IMDB for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["maps"];
			this.url = "https://www.google.com/maps?q={query}";
			this.preview = function(input) {
				return "Search Google Maps for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["scnsrc.me"];
			this.url = "https://www.scnsrc.me/";
			this.preview = function(input) {
				return "Open www.scnsrc.me";
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},
		new function() {
			var me = this;
			this.names = ["1337x.to", "x"];
			this.url = "https://1337x.to/search/{query}/1/";
			this.preview = function(input) {
				return "Search 1337x.to for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["extratorrent.si","et"];
			this.url = "https://extratorrent.si/search/?search={query}&new=1&x=0&y=0";
			this.preview = function(input) {
				return "Search extratorrent for " + join(input, " ");
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["NAS"];
			this.url = "http://192.168.1.2:5000";
			this.preview = function(input) {
				return "Open NAS"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},
		new function() {
			var me = this;
			this.names = ["qr"];
			// https://developers.google.com/chart/infographics/docs/qr_codes
			this.url = "https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl={query}";
			this.preview = function(input) {
				return "QR code for " + input;
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		new function() {
			var me = this;
			this.names = ["rutracker","ru"];
			this.url = "http://rutracker.org/forum/search_cse.php?q={query}";
			this.preview = function(input) {
				return "Search rutracker.org for " + input;
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me, join(input, " "));
			};
		},
		/*
		 *	NAS shortcuts below
		 */
		new function() {
			var me = this;
			this.names = ["sickrage"];
			this.url = "http://192.168.1.2:8081/home/";
			this.preview = function(input) {
				return "Open SickRage"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},new function() {
			var me = this;
			this.names = ["Plex"];
			this.url = "http://192.168.1.2:32400/web/";
			this.preview = function(input) {
				return "Open Plex"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},,new function() {
			var me = this;
			this.names = ["Jackett"];
			this.url = "http://192.168.1.2:9117";
			this.preview = function(input) {
				return "Open Jackett"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},,new function() {
			var me = this;
			this.names = ["Radarr"];
			this.url = "http://192.168.1.2:7878";
			this.preview = function(input) {
				return "Open Radarr"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},,new function() {
			var me = this;
			this.names = ["Sonarr"];
			this.url = "http://192.168.1.2:8989";
			this.preview = function(input) {
				return "Open Sonarr"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		},,new function() {
			var me = this;
			this.names = ["TM"];
			this.url = "http://192.168.1.2:9091";
			this.preview = function(input) {
				return "Open Transmission"
			};
			this.execute = function(input) {
				return ubiquity.getParsedUrl(me);
			};
		}
	];
	/*
	 * End of commands list
	 */
	
	this.getParsedUrl = function(command, query) {
		return command.url.replace("{query}", encodeURIComponent(query));
	}
	
	this.hasCommand = function(abbreviatedName) {
		var command = ubiquity.findCommand(abbreviatedName);
		return typeof command !== "undefined";
	};
	this.findCommand = function(abbreviatedName) {
		for (i in ubiquity.commands) {
			var c = ubiquity.commands[i];
			var nameMatches = false;
			for (j in c.names) {
				var currentName = c.names[j];
				if(currentName.toLowerCase().indexOf(abbreviatedName.toLowerCase()) !== -1) {
					nameMatches = true;
					break;
				}
			}
			if(nameMatches) {
				return c;
			}
		}
	};
	this.preview = function(abbreviatedName, input) {
		var command = ubiquity.findCommand(abbreviatedName);
		if(typeof command !== "undefined") {
			return command.preview(input);
		}
	};
	this.process = function(abbreviatedName, input) {
		var command = ubiquity.findCommand(abbreviatedName);
		if(typeof command !== "undefined") {
			return command.execute(input);
		}
	};
};
